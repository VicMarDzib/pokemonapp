import { useNavigation } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, Image, View, ActivityIndicator, SafeAreaView, FlatList, Alert } from 'react-native'

const PokemonScreen = ({ route }) => {
  const navigation = useNavigation()
  const { url } = route.params;

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState({});

  const showMove = (url) => {
    fetch(url)
      .then((response) => response.json())
      .then((response) => Alert.alert(response.effect_entries[0].effect))
      .catch((error) => alert(error));
  }

  useEffect(() => {
    let isMounted = true;
    fetch(url)
      .then((response) => response.json())
      .then((response) => setData(response))
      .catch((error) => alert(error))
      .finally(() => { setLoading(false); return () => { isMounted = false }; });

  }, [])

  return (
    <SafeAreaView style={styles.container}>

      {
        isLoading ? (
          <ActivityIndicator />
        ) : (
          <View>
            <Text style={styles.title}> Pokemon: {data.species.name}</Text>

            <Image
              style={styles.tinyLogo}
              source={{
                uri: data.sprites.other['official-artwork'].front_default,
              }}
            />

            <Text style={styles.subtitle}>Tipos de ataques</Text>
            <FlatList
              data={data.moves}
              keyExtractor={(item, index) => item.move.name}
              renderItem={
                ({ item }) => <Text style={styles.listText} onPress={() => { showMove(item.move.url) }}>{item.move.name}</Text>

              }
            />
          </View>
        )
      }

    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: 300,
    height: 300,
    alignSelf: 'center'
  },
  title: {
    fontWeight: 'bold',
    fontSize: 30
  },
  subtitle: {
    fontWeight: 'bold',
    fontSize: 25
  },
  listText: {
    fontSize: 26,
    fontWeight: '200'
  }
});

export default PokemonScreen;