import { useNavigation } from '@react-navigation/core'
import React, { useEffect, useState } from 'react'
import { StyleSheet, SafeAreaView, Text, ActivityIndicator, FlatList, View, Item } from 'react-native'

const HomeScreen = () => {

  const randomNum = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }

  const apiURL = "https://pokeapi.co/api/v2/pokemon?limit=10&offset=" + randomNum(1, 150);

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const navigation = useNavigation()

  //Obteniendo datos
  useEffect(() => {
    fetch(apiURL)
      .then((response) => response.json())
      .then((response) => setData(response.results))
      .catch((error) => alert(error))
      .finally(() => setLoading(false));

  }, [])

  const navegar = (uri) => {
    navigation.navigate("Pokemon", { url: uri });
  }
  return (
    <SafeAreaView style={styles.container}>

      {
        isLoading ? (
          <ActivityIndicator />
        ) : (
          <View style={styles.container}>
            <Text style={styles.title}>Listado de pokemons</Text>
            <FlatList
              data={data}
              keyExtractor={(item, index) => item.name}
              renderItem={
                ({ item }) => <Text style={styles.listText} onPress={() => navegar(item.url)}>{item.name}</Text>

              }
            />
          </View>
        )
      }

    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tinyLogo: {
    width: 150,
    height: 150,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 20
  },
  listText: {
    fontSize: 26,
    fontWeight: '200'
  }
});

export default HomeScreen;

